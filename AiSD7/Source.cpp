#include <iostream>
#include <vector>
#include <array>
#include <algorithm>
#include <iomanip>

void HighestProfit(std::vector<int> C)
{
	int highest_profit = 0;
	int lowest_price = C[0];
	int sell_day = 0;
	int buy_day = 0;

	for (int i = 1; i < C.size(); ++i)
	{
		if (C[i] - lowest_price > highest_profit)
		{
			highest_profit = C[i] - lowest_price;
			sell_day = i;
		}
		else if (lowest_price > C[i])
		{
			lowest_price = C[i];
			buy_day = i;
		}
	}

	std::cout << "dzien kupna: " << buy_day + 1 << " cena: " << lowest_price << "\ndzien sprzedazy: " << sell_day + 1 << " zysk: " << highest_profit << "\n\n";
}

void File(std::vector<std::array<int, 2>> t, int W, int num)
{
	//std::array<std::array<int, W>, W> result;
	std::vector<std::vector<int>> result(num, std::vector<int>(W));
	std::vector<int> inside;

	for (int j = 0; j < W; ++j)
		result[0][j] = 0;

	for (int i = 1; i < num; ++i)
	{
		for (int j = 0; j < W; ++j)
		{
			if (t[i - 1][1] <= j)
			{
				result[i][j] = std::max(result[i - 1][j], result[i - 1][j - t[i - 1][1]] + t[i - 1][0]);
			}
			else
			{
				result[i][j] = result[i - 1][j];
			}
				
		}
	}

	for (int i = 0; i < num; ++i)
	{
		for (int j = 0; j < W; ++j)
		{
			std::cout << std::setw(3) << std::left << result[i][j] << " ";
		}
		std::cout << "\n";
	}

	int i = num - 1, k = W - 1;
	while (i >0 && k > 0)
	{
		if (result[i][k] != result[i - 1][k])
		{
			inside.push_back(i);
			--i;
			k -= t[i][1];
		}
		else
		{
			--i;
		}
	}
	std::cout << "Pliki do zapisania: \n";
	for (auto &element : inside)
		std::cout << element << " ";
	std::cout << "\n";
}

int main()
{
	std::vector<int> C = { 5,10,4,11,7 };
	HighestProfit(C);

	std::vector<std::array<int, 2>> t = { { 10,3 },{ 15,8 },{ 20,6 },{ 90,3 } };

	File(t, 11, 5);

	system("pause");
}